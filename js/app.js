let todoItems = [];
   const CHECK="✔️";
  const DELETE=""
const input = document.querySelector('.input');
const list = document.querySelector('.list');
const form = document.querySelector('.submit-todo');
function addTodo(text) {
 var item;
  const todo = {
    text,
    checked: false,
    id: Date.now(),
  };

  todoItems.push(todo);
  
  console.log(todoItems);
  
       
  const position = 'beforeend';
   
         item =`

           <li class="todo-item" data-key="${todo.id}">
            <i class="fas fa-check-circle tick js-tick" id="${todo.id}"></i>
            <span class="text">${todo.text}</span>
            <i class="far fa-trash-alt delete-todo js-delete"></i>
          <hr>
          </li>
        `;
      
  list.insertAdjacentHTML(position,item);
 } 

form.addEventListener('submit', event => {
  event.preventDefault();
  const text = input.value.trim();
  if (text !== '') {
    addTodo(text);
    input.value = '';
    input.focus();
  }
});



list.addEventListener('click',event=>{
  if(event.target.classList.contains('js-tick')){
    const Key=event.target.parentElement.dataset.key;
   const index =todoItems.findIndex(item =>item.id == Number(Key));
    
   todoItems[index].checked = !todoItems[index].checked;

   const item = document.querySelector(`[data-key='${Key}']`);
   const element= document.querySelector('.tick');
   if (todoItems[index].checked) {
      item.classList.add('done');
        } 
   else {
      item.classList.remove('done');
             // return DONE;
   } 
   // document.write(`${CHECK}`);
  }

  if(event.target.classList.contains('js-delete')){
    const Key=event.target.parentElement.dataset.key;
    const index =todoItems.findIndex(item =>item.id == Number(Key));
    const item = document.querySelector(`[data-key='${Key}']`);
    item.remove();
  }

});